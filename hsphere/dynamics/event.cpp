#ifndef EVENT_CPP
#define EVENT_CPP

#define EVENT_DELTA_DBL_EPSILON (10.0 * DBL_EPSILON)

#include "particle.cpp"
#include "../math/root.cpp"

enum event_type {PARTICLE, BUMPER, SEGMENT_A, SEGMENT_MID, SEGMENT_B};

struct event
{
    event_type type;
    hfloat t;
    
    bool occurred;
    
    // Constructors
    
    event(const particle & a, const particle & b, hfloat max_t)
    {
        bool delta_test;
        
#ifdef HFLOAT_USE_GMP
        if(this->particle_double_delta_test(a, b) == YES)
            delta_test = true;
        else if(this->particle_double_delta_test(a, b) == NO)
            delta_test = false;
        else
            delta_test = this->particle_hfloat_delta_test(a, b);
#else
        delta_test = this->particle_hfloat_delta_test(a, b);
#endif
        if(delta_test)
        {
#ifdef PARTICLE_MASS
            hvector va = a.p / a.m;
            hvector vb = b.p / b.m;
#else
            hvector va = a.p;
            hvector vb = b.p;
#endif
            
#ifdef PARTICLE_RADIUS
            hfloat R = a.r + b.r;
#else
#define R hfloat("2.0")
#endif
            
            hvector xa = a.x - va * a.t;
            hvector xb = b.x - vb * b.t;
            
            hvector dx = xa - xb;
            hvector dv = va - vb;
            
            hfloat A = ~dv;
            hfloat B = hfloat("2.0") * dx * dv;
            hfloat C = ~dx - R * R;

            this->t = root_quadratic(A, B, C);
            
            if(this->t > 0 && this->t < max_t)
                this->occurred = true;
            else
                this->occurred = false;
        }
        else
            this->occurred = false;   
    }
    
private:
    
    // Private methods
    
#ifdef HFLOAT_USE_GMP
    
    enum double_test_result {YES, NO, ND};
    
    double_test_result particle_double_delta_test(const particle & a, const particle & b)
    {
        vector xa = a.x;
        vector xb = b.x;
        
        double ta = a.t.get_d();
        double tb = b.t.get_d();
        
#ifdef PARTICLE_MASS
        vector pa = a.p;
        vector pb = b.p;
        
        double ma = a.m.get_d();
        double mb = b.m.get_d();
        
        vector va = pa / ma;
        vector vb = pb / mb;
#else
        vector va = a.p;
        vector vb = b.p;
#endif
        
#ifdef PARTICLE_RADIUS
        double ra = a.r.get_d();
        double rb = a.r.get_d();
        double R = ra + rb;
#else
#define R 2.0
#endif
        
        xa -= va * ta;
        xb -= vb * tb;
        
        vector dx = xa - xb;
        vector dv = va - vb;
        
        double A = ~dv;
        double B = 2.0 * dx * dv;
        double C = ~dx - R * R;
        
        double delta = B * B - 4.0 * A * C;
        
        if(delta < - EVENT_DELTA_DBL_EPSILON)
            return NO;
        else if(delta > EVENT_DELTA_DBL_EPSILON)
            return YES;
        else
            return ND;
    }

#endif
    
    bool particle_hfloat_delta_test(const particle & a, const particle & b)
    {
#ifdef PARTICLE_MASS
        hvector va = a.p / a.m;
        hvector vb = b.p / b.m;
#else
        hvector va = a.p;
        hvector vb = b.p;
#endif
        
#ifdef PARTICLE_RADIUS
        hfloat R = a.r + b.r;
#else
#define R hfloat("2.0")
#endif
        
        hvector xa = a.x - va * a.t;
        hvector xb = b.x - vb * b.t;
        
        hvector dx = xa - xb;
        hvector dv = va - vb;
        
        hfloat A = ~dv;
        hfloat B = hfloat("2.0") * dx * dv;
        hfloat C = ~dx - R * R;
        
        hfloat delta = B * B - hfloat("4.0") * A * C;
        
        return delta > 0;
    }
};

#endif