#ifndef PARTICLE_CPP
#define PARTICLE_CPP

#define PARTICLE_MASS
#define PARTICLE_RADIUS

#include <iostream>

#include "../math/hfloat.cpp"
#include "../geometry/hvector.cpp"

using namespace std;

struct particle
{
    hvector x;
    hvector p;
    
#ifdef PARTICLE_MASS
    hfloat m;
#endif
    
#ifdef PARTICLE_RADIUS
    hfloat r;
#endif
    
    hfloat t;
    
    // Constructors
    
#ifdef PARTICLE_MASS

#ifdef PARTICLE_RADIUS
    particle(const hvector & x, const hvector & p, const hfloat & m, const hfloat & r)
    {
        this->x = x;
        this->p = p;
        this->m = m;
        this->r = r;
        this->t = 0;
    }
#else
    particle(const hvector & x, const hvector & p, const hfloat & m)
    {
        this->x = x;
        this->p = p;
        this->m = m;
        this->t = 0;
    }
#endif

#else
    
#ifdef PARTICLE_RADIUS
    particle(const hvector & x, const hvector & p, const hfloat & r)
    {
        this->x = x;
        this->p = p;
        this->r = r;
        this->t = 0;
    }
#else
    particle(const hvector & x, const hvector & p)
    {
        this->x = x;
        this->p = p;
        this->t = 0;
    }
#endif

#endif
    
    particle(const particle & p)
    {
        this->x = p.x;
        this->p = p.p;
        
#ifdef PARTICLE_MASS
        this->m = p.m;
#endif
        
#ifdef PARTICLE_RADIUS
        this->r = p.r;
#endif
        
        this->t = p.t;
    }
    
    // Operators
    
    void operator = (const particle & p)
    {
        this->x = p.x;
        this->p = p.p;
        
#ifdef PARTICLE_MASS
        this->m = p.m;
#endif
        
#ifdef PARTICLE_RADIUS
        this->r = p.r;
#endif
        
        this->t = p.t;
    }
};

#endif