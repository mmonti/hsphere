#ifndef VECTOR_CPP
#define VECTOR_CPP

#include <iostream>
#include <math.h>

using namespace std;

struct vector
{
    double x;
    double y;
    
    // Constructors
    
    vector()
    {
        this->x = 0;
        this->y = 0;
    }
    
    vector(const double & x, const double & y)
    {
        this->x = x;
        this->y = y;
    }
    
    vector(const vector & v)
    {
        this->x = v.x;
        this->y = v.y;
    }
    
    // Operators
    
    vector operator + (const vector & v) const
    {
        return vector(this->x + v.x, this->y + v.y);
    }
    
    void operator += (const vector & v)
    {
        this->x += v.x;
        this->y += v.y;
    }
    
    vector operator - (const vector & v) const
    {
        return vector(this->x - v.x, this->y - v.y);
    }
    
    void operator -= (const vector & v)
    {
        this->x -= v.x;
        this->y -= v.y;
    }
    
    double operator * (const vector & v) const
    {
        return this->x * v.x + this->y * v.y;
    }
    
    vector operator * (const double & l) const
    {
        return vector(this->x * l, this->y * l);
    }
    
    void operator *= (const double & l)
    {
        this->x *= l;
        this->y *= l;
    }
    
    vector operator / (const double & l) const
    {
        return vector(this->x / l, this->y / l);
    }
    
    void operator /= (const double & l)
    {
        this->x /= l;
        this->y /= l;
    }
    
    double operator ~ () const
    {
        return (*this) * (*this);
    }
    
    double operator ! () const
    {
        return sqrt((*this) * (*this));
    }
    
    void operator = (const vector & v)
    {
        this->x = v.x;
        this->y = v.y;
    }
};

vector operator * (const double & l, const vector & v)
{
    return v * l;
}

ostream & operator << (ostream & out, const vector & v)
{
    out<<"("<<v.x<<", "<<v.y<<")";
    return out;
}

#endif