#ifndef HVECTOR_CPP
#define HVECTOR_CPP

#include "../math/hfloat.cpp"
#include "vector.cpp"

struct hvector
{
    hfloat x;
    hfloat y;
    
    // Constructors
    
    hvector()
    {
        this->x = 0;
        this->y = 0;
    }

    hvector(const hfloat & x, const hfloat & y)
    {
        this->x = x;
        this->y = y;
    }

#ifdef HFLOAT_USE_GMP
    hvector(const double & x, const double & y)
    {
        this->x = x;
        this->y = y;
    }
#endif
    
    hvector(const hvector & v)
    {
        this->x = v.x;
        this->y = v.y;
    }
    
    hvector(const vector & v)
    {
        this->x = v.x;
        this->y = v.y;
    }
    
    // Operators
    
    hvector operator + (const hvector & v) const
    {
        return hvector(this->x + v.x, this->y + v.y);
    }
    
    void operator += (const hvector & v)
    {
        this->x += v.x;
        this->y += v.y;
    }
    
    hvector operator - (const hvector & v) const
    {
        return hvector(this->x - v.x, this->y - v.y);
    }
    
    void operator -= (const hvector & v)
    {
        this->x -= v.x;
        this->y -= v.y;
    }
    
    hfloat operator * (const hvector & v) const
    {
        return this->x * v.x + this->y * v.y;
    }
    
    hvector operator * (const hfloat & l) const
    {
        return hvector(this->x * l, this->y * l);
    }
    
    void operator *= (const hfloat & l)
    {
        this->x *= l;
        this->y *= l;
    }

#ifdef HFLOAT_USE_GMP
    hvector operator * (const double & l) const
    {
        return hvector(this->x * l, this->y * l);
    }
#endif
    
#ifdef HFLOAT_USE_GMP
    void operator *= (const double & l)
    {
        this->x *= l;
        this->y *= l;
    }
#endif
    
    hvector operator / (const hfloat & l) const
    {
        return hvector(this->x / l, this->y / l);
    }
    
    void operator /= (const hfloat & l)
    {
        this->x /= l;
        this->y /= l;
    }
    
#ifdef HFLOAT_USE_GMP
    hvector operator / (const double & l) const
    {
        return hvector(this->x / l, this->y / l);
    }
#endif
    
#ifdef HFLOAT_USE_GMP
    void operator /= (const double & l)
    {
        this->x /= l;
        this->y /= l;
    }
#endif
    
    hfloat operator ~ () const
    {
        return (*this) * (*this);
    }
    
    hfloat operator ! () const
    {
        return sqrt((*this) * (*this));
    }
    
    void operator = (const hvector & v)
    {
        this->x = v.x;
        this->y = v.y;
    }
    
    // Casting
    
    operator vector() const
    {
        return vector(this->x.get_d(), this->y.get_d());
    }
};

hvector operator * (const hfloat & l, const hvector & v)
{
    return v * l;
}

#ifdef HFLOAT_USE_GMP
hvector operator * (double l, const hvector & v)
{
    return v * l;
}
#endif

ostream & operator << (ostream & out, const hvector & v)
{
    out<<"("<<v.x<<", "<<v.y<<")";
    return out;
}

#endif