#ifndef HFLOAT_CPP
#define HFLOAT_CPP

#define HFLOAT_USE_GMP
#define HFLOAT_DEFAULT_PRECISION 16384
#define HFLOAT_RANDOM_SEED TIME

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef HFLOAT_USE_GMP
#include <gmpxx.h>
#endif

#ifdef HFLOAT_USE_GMP
typedef mpf_class hfloat;
#else
typedef double hfloat;
#endif

using namespace std;

#ifdef HFLOAT_USE_GMP
class hfloat_interface
{
    gmp_randclass * gmp_rand;
    
public:

    hfloat epsilon;
    
    // Constructor
    
    hfloat_interface()
    {
        mpf_set_default_prec(HFLOAT_DEFAULT_PRECISION);
        cout.precision(HFLOAT_DEFAULT_PRECISION);
        
        this->gmp_rand = new gmp_randclass(gmp_randinit_default);
        
#ifdef HFLOAT_RANDOM_SEED
#if HFLOAT_RANDOM_SEED == TIME
        this->gmp_rand->seed(time(NULL));
#else
        this->gmp_rand->seed(HFLOAT_RANDOM_SEED);
#endif
#endif
        
        hfloat epsilon_reference = 1;
        this->epsilon = 1;
        
        while(epsilon_reference + this->epsilon != epsilon_reference)
            this->epsilon /= 2;
        
        this->epsilon *= 2;

    }
    
    // Destructor
    
    ~hfloat_interface()
    {
        delete this->gmp_rand;
    }
    
    // Methods
    
    hfloat rand()
    {
        return this->gmp_rand->get_f();
    }
};

#else

class hfloat_interface
{
public:

    hfloat epsilon;
    
    // Constructor
    
    hfloat_interface()
    {
#ifdef HFLOAT_RANDOM_SEED
#if HFLOAT_RANDOM_SEED == TIME
        srand((unsigned int) time(NULL));
#else
        srand(HFLOAT_RANDOM_SEED);
#endif
#endif
        
        hfloat epsilon_reference = 1;
        this->epsilon = 1;
        
        while(epsilon_reference + this->epsilon != epsilon_reference)
            this->epsilon /= 2;
        
        this->epsilon *= 2;
    }
    
    // Methods

    hfloat rand()
    {
        return ((hfloat) ::rand()) / RAND_MAX;
    }
};
#endif

hfloat_interface hfloat_interface;

#endif