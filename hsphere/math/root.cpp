#ifndef ROOT_CPP
#define ROOT_CPP

#include "hfloat.cpp"

#define ROOT_QUADRATIC_EPSILON (hfloat_interface.epsilon * 5)
#define ROOT_QUADRATIC_MAX_ITERATIONS 100

hfloat root_quadratic(const hfloat &a, const hfloat &b, const hfloat &c)
{
    hfloat x = 0.;
    
    unsigned int i;
    for(i=0; i<ROOT_QUADRATIC_MAX_ITERATIONS; i++)
    {
        hfloat x2 = x * x;
        hfloat y = x - (a * x2 + b * x + c) / (hfloat("2") * a * x + b);
        
        if(x - y < ROOT_QUADRATIC_EPSILON && x - y > - ROOT_QUADRATIC_EPSILON)
            break;

        x = y;
    }
    cout<<i<<endl;
    return x;
}

#endif