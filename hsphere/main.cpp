#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "dynamics/event.cpp"
#include "math/root.cpp"

using namespace std;

int main()
{
    particle a(hvector(0, 0), hvector(1, 0), 1, 1);
    particle b(hvector(hfloat(10), hfloat("1.5") - hfloat("1.0E-50")), hvector(-1, 0), 1, 1);
    
    event c(a, b, 1000);
}
